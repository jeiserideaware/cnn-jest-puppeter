const timeout = 20000
var homeObj = require('../objects/homePageObj.js')
var searchObj = require("../objects/searchPageObj.js")
var home= new homeObj();
var search= new searchObj();

describe('When successful search',() => {

    beforeAll(async () => {
      page = await global.__BROWSER__.newPage()
      await page.goto('https://edition.cnn.com/')

      page.emulate({
        viewport: {
        width: 1280,
        height: 900
        },
        userAgent: ''
      });
      
    }, timeout)

    afterAll(async () => {
      await page.close()
    })

    it('should contain at least one result', async () => {
      await page.click(home.searchButton);
      await page.type(home.searchField, 'NFL');
      await page.click(home.submitButton);
      await page.waitForSelector(search.resultList);
    })
  },
  timeout
)

describe('When unsuccessful search',() => {

    beforeAll(async () => {
      page = await global.__BROWSER__.newPage()
      await page.goto('https://edition.cnn.com/')

      page.emulate({
        viewport: {
        width: 1280,
        height: 900
        },
        userAgent: ''
      });
      
    }, timeout)

    afterAll(async () => {
      await page.close()
    })

    it('should show no result list', async () => {
      await page.click(home.searchButton);
      await page.type(home.searchField, 'NFLFake');
      await page.click(home.submitButton);
      await page.waitForSelector(search.noResultList);
    })
  },
  timeout
)
