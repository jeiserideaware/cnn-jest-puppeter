class Search_objects {
    constructor() {
        this.resultList = '.cnn-search__results-list > .cnn-search__result';
        this.noResultList = '.cnn-search__results-list > .cnn-search__no-results';
    }
}
module.exports = Search_objects;